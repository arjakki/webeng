import { Component, OnInit} from '@angular/core';
import {HttpGateway} from './http.api.service';
import {ApiGateway,UserDataResponse} from './app.gateway';
import {FormGroup,FormBuilder, Validators, FormControl} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'new-app';
  name = "Welcome";
  userList: UserDataResponse[] = [];

  constructor (private httpGateway : HttpGateway) {}
  ngOnInit() {
    this.userList=[];
    this.getUserList();
  }

getUserList() {
  this.httpGateway.getUsers().subscribe(returnedUsers => {
    for(let  i = 0; i < returnedUsers.length; i++){
      this.userList.push(returnedUsers[i]);
    }
  })
}
}
