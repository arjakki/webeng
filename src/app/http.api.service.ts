import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiGateway,UserDataResponse} from './app.gateway';

@Injectable()
export class HttpGateway implements ApiGateway {
  constructor( private http: HttpClient ){}

getUsers(): Observable<UserDataResponse[]> {
    //return this.http.get<UserDataResponse[]>('http://localhost:8080/api/chaosapp/name');
    //return this.http.get<UserDataResponse[]>('http://3.230.151.212:8080/api/chaosapp/name');
    return this.http.get<UserDataResponse[]>('http://ServiceTier-NLB-dabab9ec1bdf240f.elb.us-east-1.amazonaws.com/api/chaosapp/name');
  }
}
