import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpGateway} from './http.api.service'
import {ApiGateway,UserDataResponse} from './app.gateway'
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule

  ],
  providers: [HttpGateway,{provide : ApiGateway, useClass:HttpGateway}],
  bootstrap: [AppComponent]
})
export class AppModule { }
