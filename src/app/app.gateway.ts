import {Observable} from 'rxjs';

export abstract class ApiGateway {
  abstract getUsers(): Observable<UserDataResponse[]>;
}


export class UserDataResponse {
  constructor(
    public id : number,
    public name : string,
    public email : string
  ){}
}
